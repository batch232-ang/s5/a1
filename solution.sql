====================================
	MySQL - CULMINATING ACTIVITY
====================================

--1. Return the customerName of the csutomers who are from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";

--2. Return the contactLastName and contactFirstName of the customers with the name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

--3. Return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

--4. Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

--5. Return the names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;

--6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" && firstName = "Steve";

--7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" && creditLimit > 3000;

--8. Return the customer numbers of orders whose comments contain the String "DHL"
SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

--9.Return the product lines whose text description mentions the phrase "state of the art"
SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

--10. Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;

--11. Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

--12. Return the customer names and countries of cutomers whose country is USA, France, or Canada
SELECT customerName FROM customers WHERE country IN ("USA", "France", "Canada");

--13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT firstName, lastName, city FROM employees
JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.city = "Tokyo";

--14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;

	--other option
SELECT customerName FROM employees
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber WHERE firstName = "Leslie" && lastName = "Thompson";

--15. Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT productName, customerName FROM orders
JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber
JOIN products ON orderdetails.productCode = products.productCode
JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customers.customerName = "Baane Mini Imports";

--16. Return the employees' first name, employee's last names, customer's names, and office's countries of transactions whose customers and offices are in the same country
SELECT DISTINCT firstName, lastName, customerName, offices.country FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
JOIN customers ON employees.employeeNumber = customers.salesRepEmployeeNumber
JOIN payments ON customers.customerNumber = payments.customerNumber WHERE customers.country = offices.country;

--17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 100
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" && quantityInStock < 1000;

--18. Return the customer's name with a phone number containing "+81"
SELECT customerName, phone FROM customers WHERE phone LIKE "+81%";



-----------------------------Stretch Goals-------------------------------

--1. Return the product name of the orders where customer name is Baane Mini Imports
SELECT productName FROM orderdetails
JOIN products ON orderdetails.productCode = products.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber WHERE customers.customerName = "Baane Mini Imports";

--2. Return the last name and first name of employees that reports to Anthony Bow
SELECT lastName, firstName FROM employees WHERE reportsTo = 1143;
	
	--other option
SELECT e2.lastName, e2.firstName FROM employees AS e1
JOIN employees AS e2 ON e1.employeeNumber = e2.reportsTo WHERE e1.firstName = "Anthony" && e1.lastName = "Bow";

--3. Return the product name of the product with the maximum MSRP
SELECT productName FROM products ORDER BY MSRP DESC LIMIT 1;

--4. Return the number of products group by productline
SELECT COUNT(productName), productLine FROM products GROUP BY productLine;

--5. Return the number of producs where the status is cancelled
SELECT COUNT(orderNumber) FROM orders WHERE status = "Cancelled";